package figury;

import java.awt.Graphics2D;
import java.awt.Polygon;
import java.awt.geom.AffineTransform;
import java.awt.geom.Area;

public class Szesciokat extends Figura
{
	public Szesciokat(Graphics2D buffer, int delay, int width, int height) 
	{
		super(buffer, delay, width, height);
		shape = new Polygon(new int[] {10,4,10,14,20,14}, new int[] {10, 20, 30,30,20,10}, 6);
		aft = new AffineTransform();
		area = new Area(shape);
	}

}
