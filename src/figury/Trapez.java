package figury;

import java.awt.geom.Area;
import java.awt.geom.AffineTransform;
import java.awt.Graphics2D;
import java.awt.Polygon;

public class Trapez extends Figura
{
	public Trapez(Graphics2D buffer, int delay, int width, int height) 
	{
		super(buffer, delay, width, height);
		shape = new Polygon(new int[] {10,20,20,10}, new int[] {5, 15, 20 ,30}, 4);
		aft = new AffineTransform();
		area = new Area(shape);
	}
	
}
